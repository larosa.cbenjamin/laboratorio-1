import random

#Esta funcion sirve para crear la apuesta de el participante
def crearapuesta ():
    
    lnumeros = []
    ncontador = 1

    while ncontador <= 3:
        numeros = input("Ingrese el primer numero de sus 3:")
        lnumeros.append(numeros)
        ncontador = ncontador + 1
    return lnumeros

#Con esta funcion se puede crear los numeros iniciales para el sorteo
def crearpool(tamaño):
    pool = []
    for i in range(tamaño):
        pool.append(i+1)

    return pool    

def main ():
    
    ganador = []
    pool = []
    lnumeros = []
    nsorteo = 0
    tamaño = 0
    premio = 0
    decision = 0
    puntaje = 0


    print("Bienvenido al sistema de Loteria de Programacion I")
    while premio != 1:
        #Este print mostrara cuantas semanas lleva el premio sin ganarse
        print("El premio lleva ", nsorteo," semanas acumuladas")
        
        #mediante este if al inicio del sorteo se le pedira la cantidad de numeros para crear la lista 
        if nsorteo == 0:
            tamaño = int(input("Porfavor ingrese los numeros totales:"))
            pool = crearpool(tamaño)
        
        #Aqui se le solicitara la creacion y confirmacion de la apuesta del participante
        while decision != 1:
            lnumeros = crearapuesta()
            print(lnumeros)
            print ("¿Confirma sus numeros?")
            print ("<0> NO")
            print ("<1> SI")
            decision = int(input())

        #Con este while se escogera un ganador        
        x = 0
        ncontador = 1
        while ncontador <= 3:
            x = random.choice(pool)
            ganador.append(x)
            pool.remove(x)
            ncontador = ncontador + 1
        
        #Aqui se suma la pool que sobro para aumentar a posibilidad de que los numeros que no han salido salgan más
        pool.extend(pool)
        pool.extend(ganador)

        #Se muestra el numero ganador con la apuesta y se genera una respuesta de victoria o empieza denuevo con 1 semana acumulada
        print(ganador)
        print(lnumeros)

        if ganador == lnumeros:
            premio = 1
            print("Felicidades usted ha ganado")
        else:    
            premio = 0
            nsorteo = nsorteo + 1
            ganador = []
            decision = 0
            print("Lo siento no hubo suerte")
            

if __name__ == "__main__":
    main()
    